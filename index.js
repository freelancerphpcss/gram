import { TelegramClient, Api } from "telegram";
import { StoreSession } from "telegram/sessions/index.js";
import _isEmpty from 'lodash/isEmpty.js';
import { CustomFile } from "telegram/client/uploads.js";
import fs from 'fs';

const userIdInBot = 219678747; // userId
const apiId = 0;
const apiHash = "";

const LIMIT_COUNT = 30;

const run = async () => {
	const client = new TelegramClient(
		new StoreSession('data'),
		apiId,
		apiHash,
		{ connectionRetries: 5 }
	);
	await client.connect();

	const clientBot = new TelegramClient(
		new StoreSession('data-bot'),
		apiId,
		apiHash,
		{ connectionRetries: 5 }
	);
	await clientBot.connect();

	const getHistory = async (offset) => {
		return await client.invoke(
			new Api.messages.GetHistory({
				peer: new Api.InputPeerChannel({
					channelId: 1788561941,
					accessHash: '16796564265763677324',
				}),
				addOffset: offset,
				limit: LIMIT_COUNT,
				minId: 768,
			})
		);
	}

	const getMessages = async () => {
		const messages = [];
		let offset = 0;

		let history = await getHistory(offset) ;
		while (!_isEmpty(history.messages)) {
			messages.push(...history.messages);
			if (history.messages.length < LIMIT_COUNT) {
				break;
			}
			offset += LIMIT_COUNT;
			history = await getHistory(offset);
		}
		return messages.reverse();
	}

	const messages = await getMessages();

	// first 4 messages its our | link for this message https://t.me/spainvlc/769
	const posts = [messages[0], messages[1], messages[2], messages[3]];

	const multiMedia = [];
	for (let index = 0; index < posts.length; index++) {
		const mediaBuffer = await client.downloadMedia(posts[index].media, {});
		const fileMedia = await clientBot.uploadFile({
			file: new CustomFile(
				`test-${index}.jpg`,
				mediaBuffer.length,
				'',
				mediaBuffer
			),
			workers: 10,
		});

		// TODO: test for sendMedia
		// await clientBot.invoke(
		// 	new Api.messages.SendMedia({
		// 		peer: userIdInBot,
		// 		media: new Api.InputMediaUploadedPhoto({
		// 			file: fileMedia,				
		// 		}),
		// 		message: posts[index].message,
		// 		entities: posts[index].entities,
		// 	})
		// );
		
		multiMedia.push(new Api.InputSingleMedia({
			media: new Api.InputMediaUploadedPhoto({
				file: fileMedia
			}),
			message: posts[index].message,
			entities: posts[index].entities,
		}));
		// fs.createWriteStream(`test-${index}.jpg`).write(mediaBuffer); // TODO: for test media
	}

	await clientBot.invoke(
		new Api.messages.SendMultiMedia({
			peer: userIdInBot,
			multiMedia,
		})
	);
}

run();